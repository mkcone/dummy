// Kind of pseudo code !!

class Something {
 compute(x, y) {
   return (x + y);
 }
}

let something = new Something();

class MyWrapper {
 constructor() {
   this.something = something;
 }
 sum(x, y) {
   return this.something.compute(x, y);
 }
}

/*
 My calls spread across my code
*/

// file1.js

let something = new MyWrapper();
console.log(something.sum(2, 3));
exports.something = something;

// file2.js
let something = require('./file1').something;
console.log(something.sum(1, 2));


/*
  If later I want to change, I just have to modify MyWrapper.
  file1.js && file2.js stay identical
*/
class MyWrapper {
 constructor() {
   this.something = require('my-super-calculator');
 }
 sum(x, y) {
   return this.something.sum(x, y);
 }
}