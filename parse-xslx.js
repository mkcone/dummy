const xlsx = require('xlsx')

let book = xlsx.readFile('file.xlsx');

/* Example */

console.log('Ex. :');
console.log('sheets', book.SheetNames);

let sheet = book.Sheets['NATPE_Budapest_2016_Buyers_List'];

console.log('First sheet && Cell A2 value', sheet['A2'].v); //A2 === 'First Name'

/* */

console.log('\n - Book analysis:');

book.SheetNames.forEach((sheetName, index) => {
  console.log('Found sheet:', sheetName);

  let sheet = book.Sheets[sheetName];

  Object.keys(sheet).forEach((position, index) => { // quick hack so we don't have to use hasOwnProp'
    let cellValue = sheet[position].v;
    console.log(`[${sheetName}][${position}] \t==\t ${cellValue} `);
  });

});


/* Output should be something like:
[NATPE_Budapest_2016_Buyers_List][C330]         ==       Director
[NATPE_Budapest_2016_Buyers_List][D330]         ==       TV Spektrum
[NATPE_Budapest_2016_Buyers_List][E330]         ==       WARSZAWA
[NATPE_Budapest_2016_Buyers_List][F330]         ==       POLAND
[NATPE_Budapest_2016_Buyers_List][A331]         ==       Jacek
[NATPE_Budapest_2016_Buyers_List][B331]         ==       Samojlowicz
[NATPE_Budapest_2016_Buyers_List][C331]         ==       CEO / International Acquisitions
[NATPE_Budapest_2016_Buyers_List][D331]         ==       Film Media S.A.
*/